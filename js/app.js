$(function()
{$("label").hover(
  function(){
    $(this).children("span").show()
  }, function(){
    $(this).children("span").hide()
});
});

$(function()
{$('div.nb-parts').change(
  function(){
    var val = $(this).children("input").val()
    console.log("val "+ val)
    var moreSlice = val % 6
    var nbrPizza = val / 6

    console.log("moreSlice "+ moreSlice)
    console.log("nbrPizza "+ nbrPizza)

    $(this).children("span.pizza-pict").remove()

    for(let i = 0; i<nbrPizza-1; i++){
      var element = $('<span class="pizza-6 pizza-pict"></span>')
      element.clone().appendTo('div.nb-parts')
    }
    if(moreSlice===0){
      var otherElement = $('<span class="pizza-6 pizza-pict"></span>')
      otherElement.clone().appendTo('div.nb-parts')
    }
    else{
      var otherElement = $('<span class="pizza-'+moreSlice+' pizza-pict"></span>')
      otherElement.clone().appendTo('div.nb-parts')
    }
  });
});

$(function(){

  var valChecked = function() {
  var extraTtl = 0
  var n = $('input[name=extra]:checked').length;
  for(let i = 0; i< n ; i++){
    extraTtl+=$('input[name=extra]:checked').eq(i).data('price')
  }
  var nbrPart = $('div.nb-parts input').val()
  var ratio = nbrPart/6
  var type = $('input[name=type]:checked').data('price') * ratio
  var pate = $('input[name=pate]:checked').data('price')
  var price = type + pate + extraTtl
  $('div.stick-right p').text(price.toFixed(2)+" €")
  }

  var goNext = function() {
  $("div.infos-client").show()
  $("button.next-step").hide()
  }

  $("button.next-step").on( "click", function(){
    valChecked();
    goNext();
  })
});


$(function()
{$("button.add").click(
  function(){
    $(this).parent().prepend("<input style='display: block' type='text'/>")
  });
});

$(function()
{$("button.done").click(
  function(){
    var name = $("div.infos-client input[type=text]").first().val()
    $("body").empty()
    alert("Merci "+name+" ! Votre commande sera livrée dans 15 minutes.")
  });
});
